package my.com.ong.advisoryappproject.model;

public class Restaurant {

    private String restaurantID, restaurantName, restaurantDistance;

    public Restaurant(String restaurantID, String restaurantName,String restaurantDistance)
    {
        this.restaurantID = restaurantID;
        this.restaurantName = restaurantName;
        this.restaurantDistance = restaurantDistance;
    }

    public String getRestaurantID() {
        return (restaurantID == null) ? "": restaurantID;
    }

    public String getRestaurantName() {
        return (restaurantName == null) ? "": restaurantName;
    }

    public String getRestaurantDistance() {
        return (restaurantDistance == null) ? "": restaurantDistance;
    }

}
