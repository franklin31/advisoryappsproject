package my.com.ong.advisoryappproject.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import my.com.ong.advisoryappproject.R;
import my.com.ong.advisoryappproject.model.Restaurant;

import java.util.ArrayList;

public class RestaurantAdapter extends RecyclerView.Adapter<RestaurantAdapter.myViewHolder>{

    private ArrayList<Restaurant> restaurantList;
    private Restaurant restaurant;

    public class myViewHolder extends RecyclerView.ViewHolder {

        TextView txtRestaurantID,txtRestaurantName,txtDistance;
        public myViewHolder(View view) {
            super(view);

            txtRestaurantID = itemView.findViewById(R.id.txtRestaurantID);
            txtRestaurantName =itemView.findViewById(R.id.txtRestaurantName);
            txtDistance =itemView.findViewById(R.id.txtDistance);

        }
    }

    public RestaurantAdapter(ArrayList<Restaurant> restaurantList){
            this.restaurantList = restaurantList;
    }

    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;

        itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.lvrow_restaurant, parent, false);


        return new myViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final myViewHolder holder, int position) {
        restaurant = restaurantList.get(position);

        holder.txtRestaurantID.setText(restaurant.getRestaurantID());
        holder.txtRestaurantName.setText(restaurant.getRestaurantName());
        holder.txtDistance.setText(restaurant.getRestaurantDistance());

    }

    @Override
    public int getItemCount() {
        return restaurantList.size();
    }
}
