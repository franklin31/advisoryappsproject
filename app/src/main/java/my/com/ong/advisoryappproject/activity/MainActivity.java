package my.com.ong.advisoryappproject.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;

import io.paperdb.Paper;
import my.com.ong.advisoryappproject.R;
import my.com.ong.advisoryappproject.adapter.RestaurantAdapter;
import my.com.ong.advisoryappproject.async.HttpRequestAsyncClass;
import my.com.ong.advisoryappproject.constant.Config;
import my.com.ong.advisoryappproject.helper.CommonHelper;
import my.com.ong.advisoryappproject.model.Restaurant;
import my.com.ong.advisoryappproject.other.RequestDividerItemDecoration;

public class MainActivity extends AppCompatActivity implements HttpRequestAsyncClass.AsyncResponse{

    private ArrayList<Restaurant> restaurantList;
    private RestaurantAdapter restaurantAdapter;
    private HttpRequestAsyncClass.AsyncResponse delegate;
    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initUI();
    }

    @Override
    public void processAsyncResponse(String result) {

        if(result.isEmpty())
        {
            CommonHelper.stopProgressBar();
            CommonHelper.showServerErrorMessage(activity);
            return;
        }

        int status = CommonHelper.getResponseStatus(activity,result);

        if(status == HttpURLConnection.HTTP_OK)
        {
            try {
                JSONObject jsonObj = new JSONObject(result);
                JSONArray resJSONArray = new JSONArray(jsonObj.getString("listing"));

                if(resJSONArray.length() == 0)
                {
                    CommonHelper.showAlertMessageDialog(activity,getResources().getString(R.string.msg_no_restaurant));
                    return;
                }

                for(int i = 0;i<resJSONArray.length();i++) {
                    JSONObject jsonObject = resJSONArray.getJSONObject(i);

                    String restaurantID = jsonObject.getString("id");
                    String restaurantName = jsonObject.getString("list_name");
                    String restaurantDistance = jsonObject.getString("distance");

                    restaurantList.add(new Restaurant(restaurantID,restaurantName,restaurantDistance));
                }

                restaurantAdapter.notifyDataSetChanged();

            }catch (JSONException e){
                CommonHelper.showErrorMessageDialog(activity,getResources().getString(R.string.msg_no_data_error));
            }
        }

        CommonHelper.stopProgressBar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                logout();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void logout()
    {
        Paper.book().write(Config.USER_ID,"");
        Paper.book().write(Config.ACCESS_TOKEN,"");

        Intent newIntent = new Intent(this, LoginActivity.class);
        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(newIntent);
    }

    private void initUI()
    {
        if(getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getResources().getString(R.string.title_restaurant));
        }

        activity = this;
        delegate = this;
        restaurantList = new ArrayList<>();
        restaurantAdapter = new RestaurantAdapter(restaurantList);

        RecyclerView rvRestaurant = findViewById(R.id.rvRestaurant);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(activity);
        rvRestaurant.setLayoutManager(mLayoutManager);
        rvRestaurant.setItemAnimator(new DefaultItemAnimator());
        rvRestaurant.addItemDecoration(new RequestDividerItemDecoration(activity, LinearLayoutManager.VERTICAL));
        rvRestaurant.setAdapter(restaurantAdapter);

        loadRestaurantData();
    }

    private void loadRestaurantData()
    {
        if(!CommonHelper.checkInternetConnection(this))
        {
            CommonHelper.showAlertMessageDialog(this,getResources().getString(R.string.msg_no_internet_connection));
            return;
        }

        HashMap<String, String> data = new HashMap<>();

        String userId = Paper.book().read(Config.USER_ID,"");
        String accessToken = Paper.book().read(Config.ACCESS_TOKEN,"");

        data.put("id", userId);
        data.put("token", accessToken);

        String postDataParams = "";

        try {
            postDataParams = CommonHelper.getPostDataString(data);
        }catch (Exception e)
        {
            e.printStackTrace();
        }

        HttpRequestAsyncClass httpReqAsyncReq = new HttpRequestAsyncClass(delegate,activity, Config.URL_LISTING);
        httpReqAsyncReq.execute(postDataParams);

    }
}
