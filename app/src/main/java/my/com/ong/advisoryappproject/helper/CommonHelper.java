package my.com.ong.advisoryappproject.helper;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;

import my.com.ong.advisoryappproject.R;

public class CommonHelper {

    private static ProgressDialog loading;

    public static void startProgressBar(Context context){
        loading = ProgressDialog.show(context, context.getResources().getString(R.string.msg_please_wait),
                context.getResources().getString(R.string.msg_loading_data), false, false);
    }

    public static void stopProgressBar(){
        loading.dismiss();
    }

    public static boolean checkInternetConnection(Context c){
        // Check Internet connection
        ConnectivityManager cn = (ConnectivityManager)c.getSystemService(
                Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf = cn.getActiveNetworkInfo();
        return nf != null && nf.isConnected();
    }

    public static void showAlertMessageDialog(Context context, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getResources().getString(R.string.msg_alert));
        builder.setMessage(message);
        builder.setPositiveButton(context.getResources().getString(R.string.msg_ok),null);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static void showErrorMessageDialog(Context context, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getResources().getString(R.string.msg_error));
        builder.setMessage(message);
        builder.setPositiveButton(context.getResources().getString(R.string.msg_ok),null);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static void showTitleMessageDialog(Context context, String message,String title){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(context.getResources().getString(R.string.msg_ok),null);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static String sendPostRequest(String requestURL, String
            postDataParams, String headerType,Context context) {
        URL url;
        String result = "";
        try {
            // Initializing url
            url = new URL(requestURL);
            // Creating an html url connection
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            // Configuring connection properties
            conn.setReadTimeout(20000);
            conn.setConnectTimeout(20000);
            // Set method as POST
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", headerType);

            conn.setDoInput(true);
            conn.setDoOutput(true);
            // Creating an output stream
            OutputStream os = conn.getOutputStream();
            // Writing parameters to the request
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                    os, "UTF-8"));
            writer.write(postDataParams);
            writer.flush();
            writer.close();
            os.close();
            // Get Response code
            int responseCode=conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {

                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String response = br.readLine();

                if(!response.isEmpty()) {
                    result = response;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String sendGetRequest(String requestURL, String
            postDataParams, String headerType,Context context) {
        URL url;
        String result = "";
        try {
            // Initializing url
            url = new URL(requestURL+"?"+postDataParams);
            // Creating an html url connection
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            // Configuring connection properties
            conn.setReadTimeout(20000);
            conn.setConnectTimeout(20000);
            // Set method as GET
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", headerType);
            conn.connect();

            // Get Response code
            int responseCode=conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {

                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String response = br.readLine();

                if(!response.isEmpty()) {
                    result = response;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public static void showServerErrorMessage(Context context)
    {
        showTitleMessageDialog(
                context,
                context.getResources().getString(R.string.msg_server_error),
                context.getResources().getString(R.string.msg_error));
    }

    public static String getPostDataString(HashMap<String, String> params) throws
            UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first) {
                first = false;
            } else {
                result.append("&");
            }
            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }
        return result.toString();
    }

    public static int getResponseStatus(Context context, String response){

        int status = HttpURLConnection.HTTP_BAD_REQUEST;
        try {
            JSONObject jsonObj = new JSONObject(response);
            JSONObject jsonStatus= jsonObj.getJSONObject("status");
            status = jsonStatus.getInt("code");

             if(status == HttpURLConnection.HTTP_BAD_REQUEST)
            {
                String message = jsonStatus.getString("message");
                showErrorMessageDialog(context,message);
            }

            return status;
        }catch (JSONException e){
            showServerErrorMessage(context);
            return status;
        }
    }

}
