package my.com.ong.advisoryappproject.constant;

public class Config {
    public static final String SERVER = "http://interview.advisoryapps.com/index.php";
    public static final String URL_LOGIN = SERVER + "/login";
    public static final String URL_LISTING = SERVER + "/listing";

    public static final String URLENCODED_HEADER = "application/x-www-form-urlencoded";
    public static final String ACCESS_TOKEN = "AccessToken";
    public static final String USER_ID = "UserID";
}
