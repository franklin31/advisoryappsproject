package my.com.ong.advisoryappproject.async;

import android.content.Context;
import android.os.AsyncTask;

import my.com.ong.advisoryappproject.constant.Config;
import my.com.ong.advisoryappproject.helper.CommonHelper;

public class HttpRequestAsyncClass extends AsyncTask<String, Void, String> {

    private HttpRequestAsyncClass.AsyncResponse delegate;
    private Context context;
    private String webApi;

    public interface AsyncResponse {
        void processAsyncResponse(String result);
    }

    public HttpRequestAsyncClass(HttpRequestAsyncClass.AsyncResponse delegate, Context context,String webApi)
    {
        this.delegate = delegate;
        this.context = context;
        this.webApi = webApi;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        CommonHelper.startProgressBar(context);
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        delegate.processAsyncResponse(result);
    }

    @Override
    protected String doInBackground(String... params) {

        String formData = params[0];

        if (webApi.equals(Config.URL_LOGIN))
            return CommonHelper.sendPostRequest(webApi,formData,Config.URLENCODED_HEADER,context);
        else
            return CommonHelper.sendGetRequest(webApi,formData,Config.URLENCODED_HEADER,context);
    }

}
