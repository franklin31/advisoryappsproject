package my.com.ong.advisoryappproject.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import io.paperdb.Paper;
import my.com.ong.advisoryappproject.R;
import my.com.ong.advisoryappproject.constant.Config;

public class LandingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);

        initUI();
    }

    private void initUI()
    {
        if (getSupportActionBar()!= null)
            getSupportActionBar().hide();

        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        startTimer();
    }

    private void startTimer()
    {
        Thread splashTimer = new Thread() {

            @Override
            public void run() {
                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {


                    String accessToken = Paper.book().read(Config.ACCESS_TOKEN,"");

                    Boolean accessRight = !accessToken.isEmpty();
                    Intent newIntent;

                    if(accessRight) {
                        newIntent = new Intent(LandingActivity.this, MainActivity.class);
                    }else
                    {
                        newIntent = new Intent(LandingActivity.this, LoginActivity.class);
                    }

                    newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(newIntent);
                }
            }
        };

        splashTimer.start();
    }
}
