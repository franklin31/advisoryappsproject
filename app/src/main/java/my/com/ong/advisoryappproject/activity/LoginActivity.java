package my.com.ong.advisoryappproject.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.HashMap;

import io.paperdb.Paper;
import my.com.ong.advisoryappproject.R;
import my.com.ong.advisoryappproject.async.HttpRequestAsyncClass;
import my.com.ong.advisoryappproject.constant.Config;
import my.com.ong.advisoryappproject.helper.CommonHelper;

public class LoginActivity extends AppCompatActivity implements HttpRequestAsyncClass.AsyncResponse{

    private String strEmail,strPassword;
    private  EditText etEmail,etPassword;
    private Activity activity;
    private HttpRequestAsyncClass.AsyncResponse delegate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initUI();
    }

    @Override
    public void processAsyncResponse(String result) {

        if(result.isEmpty())
        {
            CommonHelper.stopProgressBar();
            CommonHelper.showServerErrorMessage(activity);
            return;
        }

        int status = CommonHelper.getResponseStatus(activity,result);

        if(status == HttpURLConnection.HTTP_OK)
        {
            try {
                JSONObject jsonObj = new JSONObject(result);
                String userID = jsonObj.getString("id");
                String accessToken = jsonObj.getString("token");

                if(!userID.isEmpty() && !accessToken.isEmpty())
                {
                    Paper.book().write(Config.USER_ID,userID);
                    Paper.book().write(Config.ACCESS_TOKEN,accessToken);

                    Intent intent = new Intent(activity,MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
                else
                    CommonHelper.showErrorMessageDialog(activity,getResources().getString(R.string.msg_no_data_error));

            }catch (JSONException e){
                CommonHelper.showErrorMessageDialog(activity,getResources().getString(R.string.msg_no_data_error));
            }
        }

        CommonHelper.stopProgressBar();
    }

    private void initUI()
    {
        activity = this;
        delegate = this;
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        Button btnLogin = findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });
    }

    private boolean  loginValidation()
    {
        if(!CommonHelper.checkInternetConnection(activity))
        {
            CommonHelper.showAlertMessageDialog(activity,getResources().getString(R.string.msg_no_internet_connection));
            return false;
        }

        strEmail = etEmail.getText().toString().trim();
        strPassword = etPassword.getText().toString().trim();

        if(strEmail.isEmpty())
        {
            CommonHelper.showAlertMessageDialog(activity,getResources().getString(R.string.msg_email_empty));
            return false;
        }

        if(strPassword.isEmpty())
        {
            CommonHelper.showAlertMessageDialog(activity,getResources().getString(R.string.msg_password_empty));
            return false;
        }

        if(!CommonHelper.isEmailValid(strEmail))
        {
            CommonHelper.showAlertMessageDialog(activity,getResources().getString(R.string.msg_email_invalid));
            return false;
        }

        return true;
    }

    private void login()
    {
      if(loginValidation())
      {
          HashMap<String, String> data = new HashMap<>();

          data.put("email", strEmail);
          data.put("password", strPassword);

          String postDataParams = "";

          try {
              postDataParams = CommonHelper.getPostDataString(data);
          }catch (Exception e)
          {
              e.printStackTrace();
          }

          HttpRequestAsyncClass httpReqAsyncReq = new HttpRequestAsyncClass(delegate,activity,Config.URL_LOGIN);
          httpReqAsyncReq.execute(postDataParams);
      }
    }
}
